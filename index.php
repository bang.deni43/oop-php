<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $binatang = new animal("shaun");
    echo "name : ".$binatang->Name."<br>";
    echo "legs : ".$binatang->legs."<br>";
    echo "cold blooded : ".$binatang->cold_blooded."<br><br>";

    $kodok = new frog("buduk");
    echo "name : ".$kodok->Name."<br>";
    echo "legs : ".$kodok->legs."<br>";
    echo "cold blooded : ".$kodok->cold_blooded."<br>";
    echo $kodok->jump();
    echo "<br><br>";

    
    $sungokong = new ape("kera sakti");
    echo "name : ".$sungokong->Name."<br>";
    echo "legs : ".$sungokong->legs."<br>";
    echo "cold blooded : ".$sungokong->cold_blooded."<br>";
    echo $sungokong->yell();



?>